﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SqlStreamStore.Infrastructure;
using SqlStreamStore.Streams;

namespace Retrio
{
    public class SqlStreamRepository : IAggregateRepository
    {
        private readonly StreamStoreBase _store;
        private readonly TypeMapper _mapper;

        public SqlStreamRepository(StreamStoreBase store, TypeMapper mapper)
        {
            _store = store;
            _mapper = mapper;
        }

        public async Task<T> Get<T>(string id) where T : Aggregate, new()
        {
            var events = new List<object>();
            var version = -1;
            var page = await _store.ReadStreamForwards(new StreamId(id), StreamVersion.Start, 100);
            while (true)
            {
                foreach (var message in page.Messages)
                {
                    if (_mapper.TryGetTypeFromName(message.Type, out var type))
                    {
                        events.Add(JsonConvert.DeserializeObject(await message.GetJsonData(), type));
                    }

                    version = message.StreamVersion;
                }

                if (page.IsEnd)
                {
                    break;
                }

                page = await page.ReadNext();
            }

            if (!events.Any())
            {
                throw new InvalidOperationException("Aggregate not found.");
            }

            var aggregate = new T();
            aggregate.Load(version, events);
            return aggregate;
        }

        public Task Save<T>(T aggregate) where T : Aggregate
        {
            var events = aggregate.GetChanges();
            var streamMessages = new List<NewStreamMessage>();
            foreach (var @event in events)
            {
                _mapper.TryGetNameFromType(@event.GetType(), out var name);
                streamMessages.Add(new NewStreamMessage(Guid.NewGuid(), name, JsonConvert.SerializeObject(@event)));
            }

            return _store.AppendToStream(new StreamId(aggregate.Id.ToString()), aggregate.Version, streamMessages.ToArray());
        }

        public async Task<IEnumerable<object>> GetEventsFor(string id, int? version = null)
        {
            var events = new List<object>();
            var page = await _store.ReadStreamForwards(new StreamId(id), StreamVersion.Start, 100);

            while (true)
            {
                foreach (var message in page.Messages)
                {
                    if (message.StreamVersion == version) break;
                    if (_mapper.TryGetTypeFromName(message.Type, out var type))
                    {
                        var e = JsonConvert.DeserializeObject(await message.GetJsonData(), type);
                        events.Add(new { Data = e, message.Type });
                    }
                }

                if (page.IsEnd)
                {
                    break;
                }

                page = await page.ReadNext();
            }

            if (!events.Any())
            {
                throw new InvalidOperationException("Aggregate not found.");
            }

            return events;
        }
    }

    public class TypeMapper
    {
        private readonly Dictionary<string, Type> _nameToType = new Dictionary<string, Type>();
        private readonly Dictionary<Type, string> _typeToName = new Dictionary<Type, string>();

        public void MapType<T>(string name)
        {
            _nameToType[name] = typeof(T);
            _typeToName[typeof(T)] = name;
        }

        public bool TryGetNameFromType(Type type, out string name) => _typeToName.TryGetValue(type, out name);
        public bool TryGetTypeFromName(string name, out Type type) => _nameToType.TryGetValue(name, out type);
    }

    public interface IAggregateRepository
    {
        Task<T> Get<T>(string id) where T : Aggregate, new();
        Task Save<T>(T aggregate) where T : Aggregate;

        Task<IEnumerable<object>> GetEventsFor(string id, int? version = null);
    }

    public abstract class Aggregate
    {
        readonly IList<object> _changes = new List<object>();

        public Guid Id { get; protected set; } = Guid.Empty;
        public int Version { get; private set; } = -1;

        protected abstract void When(object e);

        protected void Apply(object e)
        {
            When(e);
            _changes.Add(e);
        }

        public void Load(int version, IEnumerable<object> history)
        {
            Version = version;
            foreach (var e in history)
            {
                When(e);
            }
        }

        public object[] GetChanges() => _changes.ToArray();
    }

}