﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Retrio.Controllers
{
    [Route("/retros")]
    [ApiController]
    public class RetrosController : ControllerBase
    {
        private readonly RetroService _service;
        private readonly RetrioContext _db;

        public RetrosController(RetroService service, RetrioContext db)
        {
            _service = service;
            _db = db;
        }

        [HttpPost("create")] public Task<ActionResult> When([FromBody]Commands.V1.CreateBoard request) => Execute(request);
        [HttpPost("title")] public Task<ActionResult> When([FromBody]Commands.V1.EditBoardTitle request) => Execute(request);
        [HttpPost("columns")] public Task<ActionResult> When([FromBody]Commands.V1.AddColumn request) => Execute(request);
        [HttpPost("column-title")] public Task<ActionResult> When([FromBody]Commands.V1.EditColumnTitle request) => Execute(request);
        [HttpPost("cards")] public Task<ActionResult> When([FromBody]Commands.V1.AddCard request) => Execute(request);
        [HttpPost("cards-contents")] public Task<ActionResult> When([FromBody]Commands.V1.UpdateCardText request) => Execute(request);
        [HttpPost("upvote")] public Task<ActionResult> When([FromBody]Commands.V1.AddVote request) => Execute(request);
        [HttpPost("downvote")] public Task<ActionResult> When([FromBody]Commands.V1.RemoveVote request) => Execute(request);
        [HttpPost("move-card")] public Task<ActionResult> When([FromBody]Commands.V1.MoveCard request) => Execute(request);
        [HttpPost("delete-card")] public Task<ActionResult> When([FromBody]Commands.V1.DeleteCard request) => Execute(request);

        private async Task<ActionResult> Execute<T>(T request) where T : class
        {
            await _service.Handle(request);
            return Ok();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById([FromRoute] Guid id)
        {
            var result = await _db.Boards
                .Where(x => x.Id == id)
                .Select(x => new
                {
                    BoardId = x.Id,
                    x.Title,
                    Columns = x.Columns.Select(y => new
                    {
                        ColumnId = y.Id,
                        y.Title,
                        Cards = y.Cards.Select(z => new
                        {
                            CardId = z.Id,
                            z.Content,
                            z.Votes,
                            z.OwnerId
                        })
                    })
                })
                .SingleOrDefaultAsync();

            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> BoardsByOwner(Guid ownerId)
        {
            var result = await _db.Boards
                .Where(b => b.OwnerId == ownerId)
                .Select(b => new
                {
                    b.Id,
                    b.Title,
                    b.Created
                }).ToListAsync();
            return Ok(result);
        }
    }
}
