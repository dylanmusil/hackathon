﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace Retrio.Controllers
{
    [Route("test")]
    [ApiController]
    public class TestsController : ControllerBase
    {
        private readonly RetrioContext _db;
        private readonly SubscriptionManager _manager;
        private readonly IHubContext<EventHub> _hub;

        public TestsController(RetrioContext db, SubscriptionManager manager, IHubContext<EventHub> hub)
        {
            _db = db;
            _manager = manager;
            _hub = hub;
        }

        // GET api/values
        [HttpGet("date")] public ActionResult<DateTime> Date() => DateTime.UtcNow;
        [HttpGet("guid")] public ActionResult<Guid> AGuid() => Guid.NewGuid();

        [HttpDelete("reset")]
        public async Task<ActionResult> ResetReadModel()
        {
            await _db.DeleteReadModel();
            await _hub.Clients.All.SendCoreAsync("DropDatabase", new object[] { "bet" });
            _manager.Start = null;
            _manager.DelayBetweenEvents = 100;
            await _manager.StopAsync(CancellationToken.None);
            await _manager.StartAsync(CancellationToken.None);

            return NoContent();
        }
    }
}