﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Retrio
{
    public class RetroService
    {
        private readonly SqlStreamRepository _store;
        private readonly ILogger<RetroService> _logger;

        public RetroService(SqlStreamRepository store, ILogger<RetroService> logger)
        {
            _store = store;
            _logger = logger;
        }

        private async Task Execute(BoardId id, Action<Retro> update)
        {
            var board = await _store.Get<Retro>(id.ToString());
            update(board);
            await _store.Save(board);
        }

        public Task Handle<T>(T request) where T : class
        {
            _logger.LogInformation(request.ToString());
            switch (request)
            {
                case Commands.V1.CreateBoard cmd:
                    {
                        var board = new Retro();
                        board.Create(cmd.BoardId, cmd.CreatedBy, cmd.Title);
                        return _store.Save(board);
                    }

                case Commands.V1.EditBoardTitle cmd:
                    return Execute(cmd.BoardId, board => board.EditBoardTitle(cmd.Title));

                case Commands.V1.AddCard cmd:
                    return Execute(cmd.BoardId, board => board.AddCard(cmd.CardId, cmd.ColumnId, cmd.UserId));

                case Commands.V1.UpdateCardText cmd:
                    return Execute(cmd.BoardId, board => board.UpdateCardText(cmd.CardId, cmd.UserId, cmd.Text, cmd.ColumnId));

                case Commands.V1.AddColumn cmd:
                    return Execute(cmd.BoardId, board => board.AddColumn(cmd.ColumnId));

                case Commands.V1.EditColumnTitle cmd:
                    return Execute(cmd.BoardId, board => board.EditColumnTitle(cmd.ColumnId, cmd.Title));

                case Commands.V1.AddVote cmd:
                    return Execute(cmd.BoardId, board => board.AddVote(cmd.CardId, cmd.UserId));

                case Commands.V1.RemoveVote cmd:
                    return Execute(cmd.BoardId, board => board.RemoveVote(cmd.CardId, cmd.UserId));

                case Commands.V1.MoveCard cmd:
                    return Execute(cmd.BoardId, board => board.MoveCard(cmd.CardId, cmd.From, cmd.To, cmd.UserId));

                case Commands.V1.DeleteCard cmd:
                    return Execute(cmd.BoardId, board => board.DeleteCard(cmd.CardId, cmd.UserId, cmd.ColumnId));

            }

            throw new ArgumentOutOfRangeException($"Can't handle command of type {typeof(T)}");
        }
    }
}