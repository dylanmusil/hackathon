using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace Retrio
{
    public class EventHub : Hub
    {
        private readonly ILogger<EventHub> _logger;
        private readonly SqlStreamRepository _repo;

        public EventHub(ILogger<EventHub> logger, SqlStreamRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public async Task JoinBoard(string boardId)
        {
            _logger.LogInformation($"Connection {Context.ConnectionId} joining board {boardId}");
            await Groups.AddToGroupAsync(Context.ConnectionId, boardId);
            await LoadBoard(boardId);
            _logger.LogInformation($"Connection {Context.ConnectionId} joined board {boardId}");
        }

        public async Task LoadBoard(string boardId, int? version = null)
        {
            if (version != default)
            {
                await Clients.Group(boardId).SendCoreAsync("DropDatabase", new object[] { "bet" });
                await Clients.Group(boardId).SendCoreAsync("Board", new object[] { await _repo.GetEventsFor(boardId, version) });
            }
            else
            {
                await Clients.Caller.SendCoreAsync("Board", new object[] {await _repo.GetEventsFor(boardId)});
            }
        }
        
        public async Task LeaveBoard(string boardId)
        {
            _logger.LogInformation($"Connection {Context.ConnectionId} leaving board {boardId}");
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, boardId);
            _logger.LogInformation($"Connection {Context.ConnectionId} left board {boardId}");
        }
    }
}