﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.Entities;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SqlStreamStore;
using SqlStreamStore.Streams;

namespace Retrio
{
    public class EventHandler
    {
        private readonly RetrioContextFactory _dbFactory;
        private readonly TypeMapper _typeMapper;
        private readonly ILogger<EventHandler> _logger;
        private readonly IHubContext<EventHub> _hub;
        private RetrioContext _db;

        public EventHandler(
            RetrioContextFactory dbFactory,
            TypeMapper typeMapper,
            ILogger<EventHandler> logger,
            IHubContext<EventHub> hub)
        {
            _dbFactory = dbFactory;
            _typeMapper = typeMapper;
            _logger = logger;
            _hub = hub;
        }

        public int Delay { get; set; }

        public async Task StreamMessageReceived(
            IAllStreamSubscription subscription,
            StreamMessage streamMessage,
            CancellationToken token)
        {
            if (Delay > 0)
            {
                await Task.Delay(Delay, token);
            }

            using (_db = _dbFactory.Create())
            {
                var index = await _db.Checkpoint.SingleAsync(token);
                if (streamMessage.Position <= index.Index)
                {
                    _logger.LogInformation($"Already saw event {streamMessage.Position}. Skipping.");
                    return;
                }

                _logger.LogInformation($"Projecting {streamMessage.Type}:{streamMessage.Position}");
                if (_typeMapper.TryGetTypeFromName(streamMessage.Type, out var type))
                {
                    try
                    {
                        var @event = JsonConvert.DeserializeObject(await streamMessage.GetJsonData(token), type);
                        await Handle((dynamic)@event);
                        var clientProxy = _hub.Clients.Group(streamMessage.StreamId);
                        await clientProxy.SendCoreAsync("EventProjected",
                            new object[] { new { Data = @event, streamMessage.Type } }, token);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError($"Failed projecting because {e}");
                    }

                    await _db.SaveChangesAsync(token);
                }
            }
        }

        private async Task Handle(Events.V1.BoardCreated @event)
        {
            var newBoard = new Board(@event.BoardId.ToGuid(), @event.Title, @event.CreatedBy.ToGuid(), @event.CreatedOn);
            await _db.Boards.AddAsync(newBoard);
        }

        private async Task Handle(Events.V1.BoardTitleEdited @event)
        {
            var board = await _db.Boards.SingleAsync(x => x.Id == @event.BoardId.ToGuid());
            board.Title = @event.Title;
        }

        private async Task Handle(Events.V1.ColumnAdded @event)
        {
            var board = await _db.Boards.SingleAsync(x => x.Id == @event.BoardId.ToGuid());
            var column = new Column(@event.ColumnId.ToGuid());
            board.Columns.Add(column);
        }

        private async Task Handle(Events.V1.ColumnTitleEdited @event)
        {
            var column = await _db.Boards
                .Where(x => x.Id == @event.BoardId.ToGuid())
                .SelectMany(x => x.Columns)
                .SingleAsync(x => x.Id == @event.ColumnId.ToGuid());
            column.Title = @event.Title;
        }

        private async Task Handle(Events.V1.CardAdded @event)
        {
            var column = await _db.Boards
                .Where(x => x.Id == @event.BoardId.ToGuid())
                .SelectMany(x => x.Columns)
                .SingleAsync(x => x.Id == @event.ColumnId.ToGuid());
            var card = new Card(@event.CardId.ToGuid(), @event.AddedBy.ToGuid());
            column.Cards.Add(card);
        }

        private async Task Handle(Events.V1.CardContentsEdited @event)
        {
            var card = await _db.Boards
                .Where(x => x.Id == @event.BoardId.ToGuid())
                .SelectMany(x => x.Columns)
                .SelectMany(x => x.Cards)
                .SingleAsync(x => x.Id == @event.CardId.ToGuid());
            card.Content = @event.Text;
        }

        private async Task Handle(Events.V1.VoteAddedToCard @event)
        {
            var card = await _db.Boards
                .Where(x => x.Id == @event.BoardId.ToGuid())
                .SelectMany(x => x.Columns)
                .SelectMany(x => x.Cards)
                .SingleAsync(x => x.Id == @event.CardId.ToGuid());
            card.Votes++;
        }

        private async Task Handle(Events.V1.VoteRemovedFromCard @event)
        {
            var card = await _db.Boards
                .Where(x => x.Id == @event.BoardId.ToGuid())
                .SelectMany(x => x.Columns)
                .SelectMany(x => x.Cards)
                .SingleAsync(x => x.Id == @event.CardId.ToGuid());
            card.Votes--;
        }

        private async Task Handle(Events.V1.CardMoved @event)
        {
            var card = await _db.Boards
                .Where(x => x.Id == @event.BoardId.ToGuid())
                .SelectMany(x => x.Columns)
                .Where(x => x.Id == @event.FromColumn.ToGuid())
                .SelectMany(x => x.Cards)
                .SingleAsync(x => x.Id == @event.CardId.ToGuid());
            card.ColumnId = @event.ToColumn.ToGuid();
        }

        private async Task Handle(Events.V1.CardDeleted e)
        {
            var column = await _db.Boards
                .Where(b => b.Id == e.BoardId.ToGuid())
                .SelectMany(b => b.Columns)
                .SingleAsync(c => c.Id == e.ColumnId.ToGuid());
            var card = column.Cards.Find(c => c.Id == e.CardId.ToGuid());
            column.Cards.Remove(card);
        }

        public void HasCaughtUp(bool hasCaughtUp)
        {
            if (hasCaughtUp)
            {
                Delay = 0;
            }
        }
    }

    public static class StringExtensions
    {
        public static Guid ToGuid(this string almostGuid)
        {
            return new Guid(almostGuid);
        }
    }
}