﻿using System;

namespace Retrio
{
    public static class Commands
    {
        public static class V1
        {
            public class CreateBoard
            {
                public Guid BoardId { get; set; }
                public string Title { get; set; }
                public Guid CreatedBy { get; set; }

                public override string ToString() => $"Creating board with id {BoardId}, title {Title}, requested by {CreatedBy}";
            }

            public class EditBoardTitle
            {
                public Guid BoardId { get; set; }
                public string Title { get; set; }
                public override string ToString() => $"Changing title board:{BoardId} to {Title}";
            }

            public class AddColumn
            {
                public Guid BoardId { get; set; }
                public Guid ColumnId { get; set; }
                public override string ToString() => $"Adding column:{ColumnId} to board:{BoardId}";
            }

            public class EditColumnTitle
            {
                public Guid ColumnId { get; set; }
                public Guid BoardId { get; set; }
                public string Title { get; set; }
                public override string ToString() => $"Changing title of column:{ColumnId} to {Title}";
            }

            public class AddCard
            {
                public Guid ColumnId { get; set; }
                public Guid BoardId { get; set; }
                public Guid CardId { get; set; }
                public Guid UserId { get; set; }
                public override string ToString() => $"Adding card to column:{ColumnId}";
            }

            public class UpdateCardText
            {
                public Guid ColumnId { get; set; }
                public Guid BoardId { get; set; }
                public Guid CardId { get; set; }
                public Guid UserId { get; set; }
                public string Text { get; set; }
                public override string ToString() => $"Changing card:{CardId} text to {Text}";
            }

            public class AddVote
            {
                public Guid BoardId { get; set; }
                public Guid CardId { get; set; }
                public Guid UserId { get; set; }
                public override string ToString() => $"Adding vote for {CardId}";
            }

            public class RemoveVote
            {
                public Guid BoardId { get; set; }
                public Guid CardId { get; set; }
                public Guid UserId { get; set; }
                public override string ToString() => $"Removing vote for {CardId}";
            }

            public class MoveCard
            {
                public Guid BoardId { get; set; }
                public Guid CardId { get; set; }
                public Guid From { get; set; }
                public Guid To { get; set; }
                public Guid UserId { get; set; }
                public override string ToString() => $"Moving card:{CardId} from column:{From} to column:{To}";
            }

            public class DeleteCard
            {
                public Guid BoardId { get; set; }
                public Guid CardId { get; set; }
                public Guid UserId { get; set; }
                public Guid ColumnId { get; set; }

                public override string ToString() => $"Deleting card:{CardId}";
            }
        }
    }
}