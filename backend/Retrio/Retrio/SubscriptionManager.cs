using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure;
using Microsoft.Extensions.Hosting;
using SqlStreamStore;
using SqlStreamStore.Infrastructure;

namespace Retrio
{
    public class SubscriptionManager : IHostedService
    {
        private readonly EventHandler _handler;
        private readonly StreamStoreBase _store;
        private static IAllStreamSubscription _sub;
        public long? Start { get; set; }
        public int DelayBetweenEvents { get; set; }

        public SubscriptionManager(EventHandler handler, StreamStoreBase store, RetrioContextFactory dbFactory)
        {
            _handler = handler;
            _store = store;
            using (var retrioContext = dbFactory.Create())
            {
                var checkpoint = retrioContext.Checkpoint.Single().Index;
                Start = checkpoint == -1 ? (long?) null : checkpoint;
                DelayBetweenEvents = 0;
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _handler.Delay = DelayBetweenEvents;
            _sub = _store.SubscribeToAll(Start, _handler.StreamMessageReceived, hasCaughtUp: _handler.HasCaughtUp);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _sub?.Dispose();
            return Task.CompletedTask;
        }
    }
}