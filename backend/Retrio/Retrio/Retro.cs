﻿using System;
using System.Collections.Generic;
using ValueOf;

namespace Retrio
{
    public class Retro : Aggregate
    {
        private readonly HashSet<ColumnId> _columns = new HashSet<ColumnId>();
        private readonly HashSet<CardId> _cards = new HashSet<CardId>();

        protected override void When(object e)
        {
            switch (e)
            {
                case Events.V1.BoardCreated bc:
                    Id = Guid.Parse(bc.BoardId);
                    break;
                case Events.V1.ColumnAdded ca:
                    _columns.Add(ColumnId.From(Guid.Parse(ca.ColumnId)));
                    break;
                case Events.V1.CardAdded ca:
                    _cards.Add(CardId.From(Guid.Parse(ca.CardId)));
                    break;
                case Events.V1.CardDeleted cd:
                    _cards.Remove(Guid.Parse(cd.CardId));
                    break;
            }
        }

        public void Create(BoardId id, UserId user, string title)
        {
            Apply(new Events.V1.BoardCreated
            {
                BoardId = id.ToString(),
                CreatedBy = user.ToString(),
                CreatedOn = DateTime.UtcNow,
                Title = title
            });
        }

        public void EditBoardTitle(string title)
        {
            Apply(new Events.V1.BoardTitleEdited
            {
                BoardId = Id.ToString(),
                Title = title,
                EditedOn = DateTime.UtcNow
            });
        }

        public void AddColumn(ColumnId id)
        {
            Apply(new Events.V1.ColumnAdded
            {
                BoardId = Id.ToString(),
                ColumnId = id.ToString(),
                AddedOn = DateTime.UtcNow
            });
        }

        public void EditColumnTitle(ColumnId column, string title)
        {
            if (!_columns.Contains(column))
            {
                throw new ColumnNotFoundException(column);
            }

            Apply(new Events.V1.ColumnTitleEdited
            {
                BoardId = Id.ToString(),
                ColumnId = column.ToString(),
                EditedOn = DateTime.UtcNow,
                Title = title
            });
        }

        public void AddCard(CardId card, ColumnId column, UserId user)
        {
            if (!_columns.Contains(column))
            {
                throw new ColumnNotFoundException(column);
            }

            Apply(new Events.V1.CardAdded
            {
                ColumnId = column.ToString(),
                BoardId = Id.ToString(),
                AddedOn = DateTime.UtcNow,
                AddedBy = user.ToString(),
                CardId = card.ToString()
            });
        }

        public void UpdateCardText(CardId card, UserId user, string text, ColumnId column)
        {
            if (!_cards.Contains(card))
            {
                throw new CardNotFoundException(card);
            }

            if (!_columns.Contains(column))
            {
                throw new ColumnNotFoundException(column);
            }

            Apply(new Events.V1.CardContentsEdited
            {
                BoardId = Id.ToString(),
                EditedOn = DateTime.UtcNow,
                EditedBy = user.ToString(),
                CardId = card.ToString(),
                Text = text,
                ColumnId = column.ToString()
            });
        }

        public void AddVote(CardId card, UserId user)
        {
            if (!_cards.Contains(card))
            {
                throw new CardNotFoundException(card);
            }

            Apply(new Events.V1.VoteAddedToCard
            {
                BoardId = Id.ToString(),
                AddedBy = user.ToString(),
                AddedOn = DateTime.UtcNow,
                CardId = card.ToString()
            });
        }

        public void RemoveVote(CardId card, UserId user)
        {
            if (!_cards.Contains(card))
            {
                throw new CardNotFoundException(card);
            }

            Apply(new Events.V1.VoteRemovedFromCard
            {
                BoardId = Id.ToString(),
                RemovedBy = user.ToString(),
                RemovedOn = DateTime.UtcNow,
                CardId = card.ToString()
            });
        }

        public void MoveCard(CardId card, ColumnId from, ColumnId to, UserId user)
        {
            if (!_columns.Contains(from))
            {
                throw new ColumnNotFoundException(from);
            }

            if (!_columns.Contains(to))
            {
                throw new ColumnNotFoundException(to);
            }

            if (!_cards.Contains(card))
            {
                throw new CardNotFoundException(card);
            }

            Apply(new Events.V1.CardMoved
            {
                BoardId = Id.ToString(),
                CardId = card.ToString(),
                FromColumn = from.ToString(),
                ToColumn = to.ToString(),
                MovedBy = user.ToString(),
                MovedOn = DateTime.UtcNow

            });
        }

        public void DeleteCard(CardId cardId, UserId userId, ColumnId columnId)
        {
            if (!_columns.Contains(columnId))
            {
                throw new ColumnNotFoundException(columnId);
            }

            if (!_cards.Contains(cardId))
            {
                throw new CardNotFoundException(cardId);
            }

            Apply(new Events.V1.CardDeleted
            {
                BoardId=Id.ToString(),
                ColumnId = columnId.ToString(),
                CardId = cardId.ToString(),
                DeletedBy = userId.ToString(),
                DeletedOn = DateTime.UtcNow,
            });
        }
    }

    public class CardNotFoundException : DomainException
    {
        private readonly CardId _card;

        public CardNotFoundException(CardId card)
        {
            _card = card;
        }
        public override string ToString()
        {
            return $"Card with id {_card} does not exist.";
        }
    }

    public class ColumnNotFoundException : DomainException
    {
        private readonly ColumnId _column;

        public ColumnNotFoundException(ColumnId column)
        {
            _column = column;
        }

        public override string ToString()
        {
            return $"Column with id {_column} does not exist.";
        }
    }

    public class DomainException : Exception
    {
    }

    public class CardId : ValueOf<Guid, CardId>
    {
        public static implicit operator Guid(CardId id) => id.Value;
        public static implicit operator CardId(Guid id) => From(id);
    }

    public class UserId : ValueOf<Guid, UserId>
    {
        public static implicit operator Guid(UserId id) => id.Value;
        public static implicit operator UserId(Guid id) => From(id);
    }


    public class BoardId : ValueOf<Guid, BoardId>
    {
        public static implicit operator Guid(BoardId id) => id.Value;
        public static implicit operator BoardId(Guid id) => From(id);
    }

    public class ColumnId : ValueOf<Guid, ColumnId>
    {
        public static implicit operator Guid(ColumnId id) => id.Value;
        public static implicit operator ColumnId(Guid id) => From(id);
    }
}