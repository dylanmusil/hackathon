﻿using System;

namespace Retrio
{
    public static class Events
    {
        public static class V1
        {
            public class BoardCreated
            {
                public string BoardId { get; set; }
                public DateTime CreatedOn { get; set; }
                public string Title { get; set; }
                public string CreatedBy { get; set; }
            }

            public class BoardTitleEdited
            {
                public string BoardId { get; set; }
                public string Title { get; set; }
                public DateTime EditedOn { get; set; }
            }

            public class ColumnAdded
            {
                public string BoardId { get; set; }
                public string ColumnId { get; set; }
                public DateTime AddedOn { get; set; }
            }

            public class ColumnTitleEdited
            {
                public string BoardId { get; set; }
                public string ColumnId { get; set; }
                public string Title { get; set; }
                public DateTime EditedOn { get; set; }
            }

            public class CardAdded
            {
                public string BoardId { get; set; }
                public string ColumnId { get; set; }
                public string CardId { get; set; }
                public DateTime AddedOn { get; set; }
                public string AddedBy { get; set; }
            }

            public class CardContentsEdited
            {
                public string BoardId { get; set; }
                public DateTime EditedOn { get; set; }
                public string EditedBy { get; set; }
                public string CardId { get; set; }
                public string Text { get; set; }
                public string ColumnId { get; set; }
            }

            public class VoteAddedToCard
            {
                public string BoardId { get; set; }
                public string AddedBy { get; set; }
                public DateTime AddedOn { get; set; }
                public string CardId { get; set; }
            }

            public class VoteRemovedFromCard
            {
                public string BoardId { get; set; }
                public string RemovedBy { get; set; }
                public DateTime RemovedOn { get; set; }
                public string CardId { get; set; }
            }

            public class CardMoved
            {
                public string BoardId { get; set; }
                public string CardId { get; set; }
                public string FromColumn { get; set; }
                public string ToColumn { get; set; }
                public string MovedBy { get; set; }
                public DateTime MovedOn { get; set; }
            }

            public class CardDeleted
            {
                public string BoardId { get; set; }
                public string ColumnId{ get; set; }
                public string CardId { get; set; }
                public string DeletedBy { get; set; }
                public DateTime DeletedOn { get; set; }
            }
        }
    }
}