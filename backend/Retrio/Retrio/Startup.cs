using Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlStreamStore;
using SqlStreamStore.Infrastructure;
using Swashbuckle.AspNetCore.Swagger;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Retrio
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(o => o.SwaggerDoc("v1", new Info { Title = "Retrio" }));

            services.AddSingleton<StreamStoreBase>(o =>
            {
                var settings = new MsSqlStreamStoreSettings(Configuration.GetConnectionString("Database"));
                var streamStore = new MsSqlStreamStore(settings);
                streamStore.CreateSchema().GetAwaiter().GetResult();
                return streamStore;
            });

            services.AddSingleton(o => new SqlStreamRepository(o.GetService<StreamStoreBase>(), o.GetService<TypeMapper>()));
            services.AddSingleton<RetroService>();

            var mapper = new TypeMapper();
            mapper.MapType<Events.V1.BoardCreated>("BoardCreated");
            mapper.MapType<Events.V1.BoardTitleEdited>("BoardTitleEdited");
            mapper.MapType<Events.V1.ColumnAdded>("ColumnAdded");
            mapper.MapType<Events.V1.ColumnTitleEdited>("ColumnTitleEdited");
            mapper.MapType<Events.V1.CardAdded>("CardAdded");
            mapper.MapType<Events.V1.CardContentsEdited>("CardContentsEdited");
            mapper.MapType<Events.V1.VoteAddedToCard>("VoteAddedToCard");
            mapper.MapType<Events.V1.VoteRemovedFromCard>("VoteRemovedFromCard");
            mapper.MapType<Events.V1.CardMoved>("CardMoved");
            mapper.MapType<Events.V1.CardDeleted>("CardDeleted");
            services.AddSingleton(mapper);
            services.AddHostedService<SubscriptionManager>();
            services.AddSignalR();
            services.AddDbContext<RetrioContext>(opt =>
                opt.UseSqlServer(Configuration.GetConnectionString("Database")));
            services.AddSingleton(o => new RetrioContextFactory(new DbContextOptionsBuilder<RetrioContext>()
                .UseSqlServer(Configuration.GetConnectionString("Database")).Options));

            services.AddSingleton<EventHandler>();
            services.AddSingleton(o => new SubscriptionManager(o.GetService<EventHandler>(),
                o.GetService<StreamStoreBase>(), o.GetService<RetrioContextFactory>()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(p =>
            {
                p.AllowAnyMethod().AllowAnyHeader().AllowCredentials();
                p.WithOrigins("http://localhost:4200",
                    "https://retrio2.firebaseapp.com",
                    "https://retrio-web.azurewebsites.net",
                    "http://retrio-web.azurewebsites.net");
            });
            app.UseHttpsRedirection();
            app.UseSignalR(o => o.MapHub<EventHub>("/events"));
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Retrio"));
            app.UseMvc();

            using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetService<RetrioContext>().Database.Migrate();
            }
        }
    }
}