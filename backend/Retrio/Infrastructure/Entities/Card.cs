﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Entities
{
    public class Card
    {
        public Card(Guid id, Guid ownerId)
        {
            Id = id;
            OwnerId = ownerId;
        }

        public Guid Id { get; set; }
        public Guid ColumnId { get; set; }
        public Guid OwnerId { get; set; }
        public string Content { get; set; }
        public int Votes { get; set; }

        public Column Column { get; set; }
    }

    public class CardMap : IEntityTypeConfiguration<Card>
    {
        public void Configure(EntityTypeBuilder<Card> builder)
        {
            builder.ToTable("Cards");
            builder.HasKey(x => x.Id);
        }
    }
}