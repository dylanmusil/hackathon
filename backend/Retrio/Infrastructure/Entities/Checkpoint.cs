﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Entities
{
    public class Checkpoint
    {
        public int Id { get; set; }
        public int Index { get; set; }
    }

    public class CheckpointMap : IEntityTypeConfiguration<Checkpoint>
    {
        public void Configure(EntityTypeBuilder<Checkpoint> builder)
        {
            builder.ToTable("Checkpoint");
            builder.HasKey(x => x.Id);
        }
    }
}