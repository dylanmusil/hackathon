﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Entities
{
    public class Column
    {
        public Column(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public Guid BoardId { get; set; }

        public Board Board { get; set; }
        public List<Card> Cards { get; set; } = new List<Card>();
    }

    public class ColumnMap : IEntityTypeConfiguration<Column>
    {
        public void Configure(EntityTypeBuilder<Column> builder)
        {
            builder.ToTable("Columns");
            builder.HasKey(x => x.Id);

            builder.HasMany(x => x.Cards)
                .WithOne(x => x.Column)
                .HasForeignKey(x => x.ColumnId);
        }
    }
}