﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Entities
{
    public class Board
    {
        public Board(Guid id, string title, Guid ownerId, DateTime created)
        {
            Id = id;
            Title = title;
            OwnerId = ownerId;
            Created = created;
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public Guid OwnerId { get; set; }
        public DateTime Created { get; set; }

        public IList<Column> Columns { get; set; } = new List<Column>();
    }

    public class BoardMap : IEntityTypeConfiguration<Board>
    {
        public void Configure(EntityTypeBuilder<Board> builder)
        {
            builder.ToTable("Boards");
            builder.HasKey(x => x.Id);

            builder.HasMany(x => x.Columns)
                .WithOne(x => x.Board)
                .HasForeignKey(x => x.BoardId);
        }
    }
}
