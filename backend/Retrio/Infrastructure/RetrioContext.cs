﻿using System.Threading;
using System.Threading.Tasks;
using Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class RetrioContextFactory
    {
        private readonly DbContextOptions<RetrioContext> _options;

        public RetrioContextFactory(DbContextOptions<RetrioContext> options)
        {
            _options = options;
        }

        public RetrioContext Create() => new RetrioContext(_options);
    }

    public class RetrioContext : DbContext
    {
        public RetrioContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Board> Boards { get; set; }
        public virtual DbSet<Checkpoint> Checkpoint { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(RetrioContext).Assembly);
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var checkPoint = await Checkpoint.SingleAsync(cancellationToken);
            checkPoint.Index++;
            return await base.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteReadModel()
        {
            Boards.RemoveRange(Boards);
            var checkpoint = await Checkpoint.SingleAsync();
            checkpoint.Index = -1;
            await base.SaveChangesAsync();

        }
    }
}