import { Injectable } from '@angular/core';

import uuid from 'uuid/v4';
import { storageKeys } from './constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userGuid: string;

  constructor() {}

  getUserGuid(): string {
    if (!this.userGuid) {
      this.userGuid = localStorage.getItem(storageKeys.userGuid);

      if (!this.userGuid) {
        this.userGuid = uuid();
        localStorage.setItem(storageKeys.userGuid, this.userGuid);
      }
    }

    return this.userGuid;
  }
}
