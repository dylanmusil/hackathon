import { Injectable, Inject } from '@angular/core';
import { Store } from '../store';

export type Theme = 'default' | 'light' | 'hotdog' | 'pabst';

interface Action {
  readonly type: 'UPDATE';
  payload: Theme;
}

function reducer(state: Theme, action: Action) {
  switch (action.type) {
    case 'UPDATE':
      return action.payload;
    default:
      return state;
  }
}

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private store = new Store(reducer, 'default' as Theme);

  theme$ = this.store.select(state => state);

  constructor() {
    // using settimeout to avoid blocking, because localStorage is sloooooooow.
    setTimeout(() => {
      const storedTheme = window.localStorage.getItem('theme');
      if (storedTheme) {
        this.changeTheme(storedTheme as Theme);
      }
    }, 0);
  }

  changeTheme(theme: Theme) {
    this.store.dispatch({ type: 'UPDATE', payload: theme });

    setTimeout(() => {
      window.localStorage.setItem('theme', theme);
    }, 0);
  }
}
