import { Component } from '@angular/core';
import { ThemeService, Theme } from '../theme.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'nav[navbar]',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  showThemeOptions = false;

  constructor(private themeService: ThemeService) {}

  toggleThemeOptions() {
    this.showThemeOptions = !this.showThemeOptions;
  }

  chooseTheme(theme: Theme) {
    this.showThemeOptions = false;
    this.themeService.changeTheme(theme);
  }
}
