export interface CreateBoard {
  boardId: string;
  title: string;
  createdBy: string;
}

export interface EditBoardTitle {
  boardId: string;
  title: string;
}

export interface AddColumn {
  boardId: string;
  columnId: string;
}

export interface EditColumnTitle {
  columnId: string;
  boardId: string;
  title: string;
}

export interface AddCard {
  columnId: string;
  boardId: string;
  cardId: string;
  userId: string;
}

export interface UpdateCardText {
  columnId: string;
  boardId: string;
  cardId: string;
  userId: string;
  text: string;
}

export interface AddVote {
  boardId: string;
  cardId: string;
  userId: string;
}

export interface RemoveVote {
  boardId: string;
  cardId: string;
  userId: string;
}

export interface MoveCard {
  boardId: string;
  cardId: string;
  from: string;
  to: string;
  userId: string;
}

export interface DeleteCard {
  boardId: string;
  cardId: string;
  columnId: string;
  userId: string;
}
