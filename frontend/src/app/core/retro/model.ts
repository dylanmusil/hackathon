export interface Card {
  id: string;
  content: string;
  userId: string;
  votes: number;
}

export interface Column {
  id: string;
  title: string;
  cardIds: string[];
}

export interface Board {
  id: string;
  title: string;
  ownerId: string;
  columnIds: string[];
}
