import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import uuid from 'uuid/v4';

import { Store } from 'src/app/store';
import { environment as env } from 'src/environments/environment';
import { Column, Card, Board } from './model';
import {
  AddColumn,
  EditColumnTitle,
  AddCard,
  UpdateCardText,
  CreateBoard,
  AddVote,
  MoveCard,
  DeleteCard
} from './commands';
import { RetroHubService } from './retro-hub.service';
import { skipWhile, first, flatMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

type Action =
  | DatabaseDropped
  | ColumnAdded
  | ColumnFailedToSave
  | ColumnTitleUpdated
  | ColumnTitleFailedToUpdate
  | CardAdded
  | CardFailedToSave
  | CardContentUpdated
  | CardContentFailedToUpdate
  | BoardCreated
  | BoardJoined
  | VoteAdded
  | VoteRemoved
  | CardMoved
  | CardDeleted;

interface State {
  board: Board;
  columns: { [id: string]: Column };
  cards: { [id: string]: Card };
}

const initialState: State = {
  board: null,
  columns: {},
  cards: {}
};

function cardReducer(card: Card, action: Action) {
  switch (action.type) {
    case 'CARD_CONTENT_UPDATED':
      return { ...card, content: action.payload.text };
    case 'CARD_CONTENT_FAILED_TO_UPDATE':
      return { ...card, content: action.payload.oldContent };
    case 'VOTE_ADDED':
      return { ...card, votes: card.votes + 1 };
    case 'VOTE_REMOVED':
      return { ...card, votes: card.votes - 1 };
    default:
      return card;
  }
}

function cardsReducer(cards: { [id: string]: Card }, action: Action) {
  switch (action.type) {
    case 'DATABASE_DROPPED':
      return {};
    case 'CARD_ADDED': {
      if (cards[action.payload.cardId]) {
        return cards;
      }

      const newCard: Card = {
        id: action.payload.cardId,
        userId: action.payload.userId,
        content: null,
        votes: 0
      };

      return { ...cards, [newCard.id]: newCard };
    }
    case 'CARD_DELETED': {
      delete cards[action.payload.cardId];
      return { ...cards };
    }
    case 'CARD_CONTENT_UPDATED':
    case 'CARD_CONTENT_FAILED_TO_UPDATE':
    case 'VOTE_ADDED':
    case 'VOTE_REMOVED':
      return {
        ...cards,
        [action.payload.cardId]: cardReducer(cards[action.payload.cardId], action)
      };
    default:
      return cards;
  }
}

function columnReducer(column: Column, action: Action): Column {
  switch (action.type) {
    case 'COLUMN_TITLE_UPDATED':
      return { ...column, title: action.payload.title };
    case 'COLUMN_TITLE_FAILED_TO_UPDATE':
      return { ...column, title: action.payload.oldTitle };
    case 'CARD_ADDED': {
      if (column.cardIds.find(id => id === action.payload.cardId)) {
        return column;
      }

      return { ...column, cardIds: [...column.cardIds, action.payload.cardId] };
    }
    case 'CARD_FAILED_TO_SAVE': {
      const index = column.cardIds.findIndex(id => id === action.payload.cardId);
      const cardIds = [...column.cardIds.slice(0, index), ...column.cardIds.slice(index + 1)];

      return { ...column, cardIds };
    }
    case 'CARD_MOVED': {
      const index = column.cardIds.findIndex(id => id === action.payload.cardId);

      if (action.payload.to === column.id && index === -1) {
        return { ...column, cardIds: [...column.cardIds, action.payload.cardId] };
      } else if (action.payload.from === column.id && index !== -1) {
        const cardIds = [...column.cardIds.slice(0, index), ...column.cardIds.slice(index + 1)];

        return { ...column, cardIds };
      }

      // in case something goofy happened
      return column;
    }
    case 'CARD_DELETED': {
      const index = column.cardIds.findIndex(id => id === action.payload.cardId);
      column.cardIds.splice(index, 1);
      return { ...column, cardIds: column.cardIds };
    }
    default:
      return column;
  }
}

function columnsReducer(columns: { [id: string]: Column }, action: Action) {
  switch (action.type) {
    case 'DATABASE_DROPPED':
      return {};
    case 'COLUMN_ADDED': {
      if (columns[action.payload.columnId]) {
        return columns;
      }

      const newColumn: Column = {
        id: action.payload.columnId,
        title: null,
        cardIds: []
      };

      return { ...columns, [newColumn.id]: newColumn };
    }
    case 'COLUMN_FAILED_TO_SAVE': {
      const { [action.payload.columnId]: col, ...rest } = columns;

      return rest;
    }
    case 'CARD_MOVED': {
      const from = columns[action.payload.from];
      const to = columns[action.payload.to];
      return { ...columns, [from.id]: columnReducer(from, action), [to.id]: columnReducer(to, action) };
    }
    case 'COLUMN_TITLE_UPDATED':
    case 'COLUMN_TITLE_FAILED_TO_UPDATE':
    case 'CARD_ADDED':
    case 'CARD_DELETED':
    case 'CARD_FAILED_TO_SAVE': {
      const column = columns[action.payload.columnId];
      if (!column) {
        return columns;
      }

      return { ...columns, [column.id]: columnReducer(column, action) };
    }
    default:
      return columns;
  }
}

function boardReducer(board: Board, action: Action) {
  switch (action.type) {
    case 'DATABASE_DROPPED':
      return { ...board, title: '', columnIds: [] };
    case 'BOARD_CREATED':
      return {
        id: action.payload.boardId,
        title: action.payload.title,
        ownerId: action.payload.createdBy,
        columnIds: []
      };
    case 'BOARD_JOINED':
      return {
        id: action.payload,
        title: null,
        ownerId: null,
        columnIds: []
      };
    case 'COLUMN_ADDED':
      if (board.columnIds.find(id => id === action.payload.columnId)) {
        return board;
      }

      return {
        ...board,
        columnIds: [...board.columnIds, action.payload.columnId]
      };
    case 'COLUMN_FAILED_TO_SAVE': {
      const index = board.columnIds.findIndex(id => id === action.payload.columnId);
      const columnIds = [...board.columnIds.slice(0, index), ...board.columnIds.slice(index + 1)];

      return { ...board, columnIds };
    }
    default:
      return board;
  }
}

function reducer(state: State, action: Action): State {
  switch (action.type) {
    default:
      return {
        board: boardReducer(state.board, action),
        columns: columnsReducer(state.columns, action),
        cards: cardsReducer(state.cards, action)
      };
  }
}

@Injectable({
  providedIn: 'root'
})
export class RetroService {
  private store = new Store(reducer, initialState);

  title$ = this.store.select(state => (state.board ? state.board.title : null));
  columns$ = this.store.select(state => {
    if (!state.board) {
      return [];
    }

    return state.board.columnIds.map(id => {
      const column = state.columns[id];

      return {
        ...column,
        cards: column.cardIds.map(cardId => state.cards[cardId])
      };
    });
  });

  // TODO: Put this stuff in the store
  private currentEventIndex = 0;
  private maxEventIndex = 0;
  get version(): number {
    return this.currentEventIndex;
  }

  get maxVersion(): number {
    return this.maxEventIndex;
  }

  private snapshot: State;

  constructor(private http: HttpClient, private retroHub: RetroHubService) {
    this.store
      .select(state => state)
      .subscribe(board => {
        this.snapshot = board;
      });

    this.retroHub.onBoard(events => {
      events.forEach(e => this.handle(e));
    });

    this.retroHub.onEventProjected(event => {
      this.handle(event);
    });

    this.retroHub.onDropDatabase(() => {
      this.store.dispatch({
        type: 'DATABASE_DROPPED'
      });
      this.currentEventIndex = 0;
    });
  }

  loadVersion(version: number) {
    const id = this.snapshot.board.id;
    this.retroHub.loadBoard(id, version);
  }

  belongsTo(userId: string): Observable<boolean> {
    return this.store.select(state => (state.board ? state.board.ownerId === userId : false));
  }

  handle(e: { data: any; type: string }) {
    this.currentEventIndex++;
    this.maxEventIndex = Math.max(this.currentEventIndex, this.maxEventIndex);
    switch (e.type) {
      case 'BoardCreated':
        this.store.dispatch({
          type: 'BOARD_CREATED',
          payload: e.data
        });
        break;
      case 'ColumnAdded':
        this.store.dispatch({
          type: 'COLUMN_ADDED',
          payload: e.data
        });
        break;
      case 'ColumnTitleEdited':
        this.store.dispatch({
          type: 'COLUMN_TITLE_UPDATED',
          payload: e.data
        });
        break;
      case 'CardAdded':
        this.store.dispatch({
          type: 'CARD_ADDED',
          payload: {
            ...e.data,
            userId: e.data.addedBy
          }
        });
        break;
      case 'CardContentsEdited':
        this.store.dispatch({
          type: 'CARD_CONTENT_UPDATED',
          payload: e.data
        });
        break;
      case 'CardContentsEdited':
        this.store.dispatch({
          type: 'CARD_CONTENT_UPDATED',
          payload: e.data
        });
        break;
      case 'VoteAddedToCard':
        this.store.dispatch({
          type: 'VOTE_ADDED',
          payload: e.data
        });
        break;
      case 'VoteRemovedFromCard':
        this.store.dispatch({
          type: 'VOTE_REMOVED',
          payload: e.data
        });
        break;
      case 'CardMoved':
        this.store.dispatch({
          type: 'CARD_MOVED',
          payload: {
            ...e.data,
            from: e.data.fromColumn,
            to: e.data.toColumn,
            userId: e.data.movedBy
          }
        });
        break;
      case 'CardDeleted':
        this.store.dispatch({
          type: 'CARD_DELETED',
          payload: e.data
        });
        break;
      default:
        break;
    }
  }

  joinBoard(boardId: string) {
    if (this.snapshot.board === null || boardId !== this.snapshot.board.id) {
      this.store.dispatch({ type: 'BOARD_JOINED', payload: boardId });

      // TODO: abstract this nonsense away into the hub service, because
      // whe should probably do this on every send/invoke to avoid errors
      this.retroHub.connected$
        .pipe(
          skipWhile(connected => !connected),
          first(),
          flatMap(() => this.retroHub.joinBoard(boardId))
        )
        .subscribe(
          () => {},
          () => {
            // TODO: do something useful if joining fails.
            // tslint:disable-next-line:quotemark
            alert("Sometimes things just don't go your way :(");
          }
        );
    }
  }

  leaveBoard() {
    if (this.snapshot.board) {
      this.retroHub.leaveBoard(this.snapshot.board.id);
    }
  }

  addColumn() {
    const cmd: AddColumn = {
      boardId: this.snapshot.board.id,
      columnId: uuid()
    };

    this.store.dispatch({
      type: 'COLUMN_ADDED',
      payload: cmd
    });

    this.http.post<unknown>(`${env.server}/retros/columns`, cmd).subscribe(
      () => {},
      () => {
        this.store.dispatch({
          type: 'COLUMN_FAILED_TO_SAVE',
          payload: {
            columnId: cmd.columnId
          }
        });
      }
    );
  }

  updateColumnTitle(columnId: string, change: { oldTitle?: string; newTitle: string }) {
    const cmd: EditColumnTitle = {
      boardId: this.snapshot.board.id,
      columnId,
      title: change.newTitle
    };

    this.store.dispatch({
      type: 'COLUMN_TITLE_UPDATED',
      payload: cmd
    });

    this.http.post<unknown>(`${env.server}/retros/column-title`, cmd).subscribe(
      () => {},
      () => {
        this.store.dispatch({
          type: 'COLUMN_TITLE_FAILED_TO_UPDATE',
          payload: {
            columnId,
            oldTitle: change.oldTitle
          }
        });
      }
    );
  }

  addCard(columnId: string, userId: string) {
    const cmd: AddCard = {
      boardId: this.snapshot.board.id,
      columnId,
      userId,
      cardId: uuid()
    };

    this.store.dispatch({
      type: 'CARD_ADDED',
      payload: cmd
    });

    this.http.post<unknown>(`${env.server}/retros/cards`, cmd).subscribe(
      () => {},
      () => {
        this.store.dispatch({
          type: 'CARD_FAILED_TO_SAVE',
          payload: {
            cardId: cmd.cardId,
            columnId
          }
        });
      }
    );
  }

  updateCardContent(
    columnId: string,
    cardId: string,
    userId: string,
    change: { oldContent: string; newContent: string }
  ) {
    const cmd: UpdateCardText = {
      boardId: this.snapshot.board.id,
      columnId, // wahhhh we don't really need this anymore but I was a dummy before.
      cardId,
      userId,
      text: change.newContent
    };

    this.store.dispatch({
      type: 'CARD_CONTENT_UPDATED',
      payload: cmd
    });

    this.http.post<unknown>(`${env.server}/retros/cards-contents`, cmd).subscribe(
      () => {},
      () => {
        this.store.dispatch({
          type: 'CARD_CONTENT_FAILED_TO_UPDATE',
          payload: {
            columnId,
            cardId,
            oldContent: change.oldContent
          }
        });
      }
    );
  }

  upvote(cardId: string, userId: string) {
    const cmd: AddVote = {
      boardId: this.snapshot.board.id,
      cardId,
      userId
    };

    this.http.post<unknown>(`${env.server}/retros/upvote`, cmd).subscribe();
  }

  downvote(cardId: string, userId: string) {
    const cmd: AddVote = {
      boardId: this.snapshot.board.id,
      cardId,
      userId
    };

    this.http.post<unknown>(`${env.server}/retros/downvote`, cmd).subscribe();
  }

  moveCard(cardId: string, from: string, to: string, userId: string) {
    const cmd: MoveCard = {
      boardId: this.snapshot.board.id,
      cardId,
      from,
      to,
      userId
    };

    this.store.dispatch({
      type: 'CARD_MOVED',
      payload: cmd
    });

    this.http.post<unknown>(`${env.server}/retros/move-card`, cmd).subscribe(
      () => {},
      () => {
        this.store.dispatch({
          type: 'CARD_MOVED',
          payload: {
            ...cmd,
            // just reverse the operation!
            from: cmd.to,
            to: cmd.from
          }
        });
      }
    );
  }

  deleteCard(cardId: string, userId: string, columnId: string) {
    const cmd: DeleteCard = {
      boardId: this.snapshot.board.id,
      cardId,
      userId,
      columnId
    };

    this.store.dispatch({
      type: 'CARD_DELETED',
      payload: cmd
    });

    this.http.post<unknown>(`${env.server}/retros/delete-card`, cmd).subscribe();
  }
}

interface DatabaseDropped {
  readonly type: 'DATABASE_DROPPED';
}

interface BoardJoined {
  readonly type: 'BOARD_JOINED';
  payload: string;
}

interface ColumnAdded {
  readonly type: 'COLUMN_ADDED';
  payload: AddColumn;
}

interface ColumnFailedToSave {
  readonly type: 'COLUMN_FAILED_TO_SAVE';
  payload: {
    columnId: string;
  };
}

interface ColumnTitleUpdated {
  readonly type: 'COLUMN_TITLE_UPDATED';
  payload: EditColumnTitle;
}

interface ColumnTitleFailedToUpdate {
  readonly type: 'COLUMN_TITLE_FAILED_TO_UPDATE';
  payload: {
    columnId: string;
    oldTitle?: string;
  };
}

interface CardAdded {
  readonly type: 'CARD_ADDED';
  payload: AddCard;
}

interface CardFailedToSave {
  readonly type: 'CARD_FAILED_TO_SAVE';
  payload: {
    columnId: string;
    cardId: string;
  };
}

interface CardContentUpdated {
  readonly type: 'CARD_CONTENT_UPDATED';
  payload: UpdateCardText;
}

interface CardContentFailedToUpdate {
  readonly type: 'CARD_CONTENT_FAILED_TO_UPDATE';
  payload: {
    columnId: string;
    cardId: string;
    oldContent: string;
  };
}

interface BoardCreated {
  readonly type: 'BOARD_CREATED';
  payload: CreateBoard;
}

interface VoteAdded {
  readonly type: 'VOTE_ADDED';
  payload: AddVote;
}

interface VoteRemoved {
  readonly type: 'VOTE_REMOVED';
  payload: AddVote;
}

interface CardMoved {
  readonly type: 'CARD_MOVED';
  payload: MoveCard;
}

interface CardDeleted {
  readonly type: 'CARD_DELETED';
  payload: DeleteCard;
}
