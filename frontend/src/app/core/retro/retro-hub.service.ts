import { Injectable } from '@angular/core';
import { HubConnectionBuilder } from '@aspnet/signalr';

import { environment as env } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RetroHubService {
  private hub = new HubConnectionBuilder().withUrl(`${env.server}/events`).build();

  private connectionSubject$ = new BehaviorSubject(false);
  connected$ = this.connectionSubject$.pipe(shareReplay(1));

  constructor() {
    this.hub.start().then(() => {
      this.connectionSubject$.next(true);
    });

    this.hub.onclose(() => {
      this.connectionSubject$.next(false);
    });
  }

  onBoard(cb: (events: { type: string; data: any }[]) => void): () => void {
    this.hub.on('Board', cb);

    return () => this.hub.off('Board', cb);
  }

  loadBoard(boardId: string, desiredVersion: number): Promise<void> {
    return this.hub.send('LoadBoard', boardId, desiredVersion);
  }

  onEventProjected(cb: (event: { type: string; data: any }) => void): () => void {
    this.hub.on('EventProjected', cb);

    return () => this.hub.off('EventProjected', cb);
  }

  onDropDatabase(cb: () => void): () => void {
    this.hub.on('DropDatabase', cb);

    return () => this.hub.off('DropDatabase', cb);
  }

  joinBoard(boardId: string): Promise<void> {
    return this.hub.send('JoinBoard', boardId);
  }

  leaveBoard(boardId: string): Promise<void> {
    return this.hub.send('LeaveBoard', boardId);
  }
}
