import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: './new-board-page/new-board-page.module#NewBoardPageModule'
  },
  {
    path: ':boardId',
    loadChildren: './board-page/board-page.module#BoardPageModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
