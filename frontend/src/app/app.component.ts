import { Component, OnInit, Renderer2, Inject } from '@angular/core';

import { ThemeService, Theme } from './core/theme.service';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private currentTheme: Theme | null = null;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    private themeService: ThemeService
  ) {}

  ngOnInit() {
    this.themeService.theme$.subscribe(theme => {
      this.renderer.addClass(this.document.body, theme);
      if (this.currentTheme) {
        this.renderer.removeClass(this.document.body, this.currentTheme);
      }
      this.currentTheme = theme;
    });
  }
}
