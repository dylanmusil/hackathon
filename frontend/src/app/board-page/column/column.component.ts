import { Component, Input, Output, EventEmitter, ElementRef, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { Column } from '../../core/retro/model';

export interface TitleChangeEvent {
  oldTitle?: string;
  newTitle: string;
}

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColumnComponent {
  editing = false;

  @Input() column: Column;

  @Output() titleUpdated = new EventEmitter<TitleChangeEvent>();
  @Output() cardAdded = new EventEmitter<void>();

  @ViewChild('titleInput') set titleInput(el: ElementRef) {
    if (el) {
      el.nativeElement.focus();
    }
  }

  formGroup = this.fb.group({
    title: ['', Validators.required]
  });

  get title() {
    return this.formGroup.get('title');
  }

  constructor(private fb: FormBuilder) {}

  edit() {
    this.editing = true;
    this.title.setValue(this.column.title || '');
  }

  save() {
    if (this.formGroup.valid) {
      this.titleUpdated.emit({
        oldTitle: this.column.title,
        newTitle: this.title.value
      });
    }

    this.editing = false;
  }

  addCard() {
    this.cardAdded.emit();
  }
}
