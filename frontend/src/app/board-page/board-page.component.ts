import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RetroService } from '../core/retro/retro.service';
import { TitleChangeEvent } from './column/column.component';
import { UserService } from '../core/user.service';
import { ContentChangeEvent } from './card/card.component';
import { CdkDragDrop } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-board-page',
  templateUrl: './board-page.component.html',
  styleUrls: ['./board-page.component.css']
})
export class BoardPageComponent implements OnInit, OnDestroy {
  boardId = this.route.snapshot.paramMap.get('boardId');
  userId = this.userService.getUserGuid();

  columns$ = this.retroService.columns$;
  title$ = this.retroService.title$;
  myBoard$ = this.retroService.belongsTo(this.userId);

  get eventIndex(): number {
    return this.retroService.version;
  }

  get maxEvent(): number {
    return this.retroService.maxVersion || 1;
  }

  constructor(private retroService: RetroService, private route: ActivatedRoute, private userService: UserService) {}

  ngOnInit() {
    this.retroService.joinBoard(this.boardId);
  }

  ngOnDestroy() {
    this.retroService.leaveBoard();
  }

  addColumn() {
    this.retroService.addColumn();
  }

  updateColumnTitle(columnId: string, change: TitleChangeEvent) {
    this.retroService.updateColumnTitle(columnId, change);
  }

  addCard(columnId: string) {
    this.retroService.addCard(columnId, this.userId);
  }

  updateCardContent(columnId: string, cardId: string, change: ContentChangeEvent) {
    this.retroService.updateCardContent(columnId, cardId, this.userId, change);
  }

  upvoteCard(cardId: string) {
    this.retroService.upvote(cardId, this.userId);
  }

  downvoteCard(cardId: string) {
    this.retroService.downvote(cardId, this.userId);
  }

  moveCard(event: CdkDragDrop<string>) {
    if (event.container !== event.previousContainer) {
      this.retroService.moveCard(event.item.data, event.previousContainer.data, event.container.data, this.userId);
    }

    // ignore changes within same column for now
  }

  deleteCard(cardId: string, columnId: string) {
    this.retroService.deleteCard(cardId, this.userId, columnId);
  }

  onEventIndexChanged(version: number) {
    this.retroService.loadVersion(version);
  }

  trackById({ id }: { id: string }) {
    return id;
  }
}
