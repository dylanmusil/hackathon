import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextFieldModule } from '@angular/cdk/text-field';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { BoardPageRoutingModule } from './board-page-routing.module';
import { BoardPageComponent } from './board-page.component';
import { ColumnComponent } from './column/column.component';
import { CardComponent } from './card/card.component';
import { ScrubberComponent } from './scrubber/scrubber.component';

@NgModule({
  declarations: [BoardPageComponent, ColumnComponent, CardComponent, ScrubberComponent],
  imports: [CommonModule, BoardPageRoutingModule, ReactiveFormsModule, TextFieldModule, DragDropModule, FormsModule]
})
export class BoardPageModule {}
