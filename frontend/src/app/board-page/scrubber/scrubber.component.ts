import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  Output,
  ViewChild
} from '@angular/core';

export interface ScrubberValueChangeEvent {
  previous: number;
  value: number;
}

@Component({
  selector: 'app-scrubber',
  templateUrl: './scrubber.component.html',
  styleUrls: ['./scrubber.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScrubberComponent {
  @Input() min = 0;
  @Input() max = 1;

  position = 0;

  // tslint:disable-next-line:variable-name
  _value = 0;
  @Input() set value(value: number) {
    this._value = value;

    if (!this.mouseDown) {
      this.position = this.mapValueToPosition(value);
    }
  }
  @Output() valueChange = new EventEmitter<ScrubberValueChangeEvent>();

  @ViewChild('track') trackEl!: ElementRef;

  private mouseDown = false;

  onMouseDown(clientX: number) {
    this.mouseDown = true;
    this.update(clientX);
  }

  @HostListener('document:mouseup', ['$event.clientX'])
  onMouseUp(clientX: number) {
    if (this.mouseDown) {
      this.mouseDown = false;
      this.update(clientX, true);
    }
  }

  @HostListener('document:mousemove', ['$event.clientX'])
  onMouseMove(clientX: number) {
    if (this.mouseDown) {
      this.update(clientX);
      event.preventDefault(); // prevent selecting text
    }
  }

  onTrackClick(clientX: number) {
    if (!this.mouseDown) {
      this.update(clientX, true);
    }
  }

  onKeyUp(key: string) {
    let offset = 0;

    switch (key) {
      case 'ArrowLeft':
        offset = -1;
        break;
      case 'ArrowRight':
        offset = 1;
        break;
      default:
        return;
    }

    const value = Math.min(this.max, Math.max(this.min, this._value + offset));
    this.updateValue(value);
  }

  private update(newX: number, commit = false) {
    const { width, x } = this.trackEl.nativeElement.getBoundingClientRect();

    const position = Math.min(Math.max((newX - x) / width, 0), 1);
    const value = this.mapPositionToValue(position);

    if (commit) {
      this.position = this.mapValueToPosition(value);
    } else {
      this.position = position;
    }

    this.updateValue(value);
  }

  private updateValue(value: number) {
    if (value === this._value) {
      // nothing has actually changed no need to emit
      return;
    }

    const previous = this._value;
    this._value = value;

    this.valueChange.emit({
      previous,
      value
    });
  }

  private mapPositionToValue(position: number): number {
    const value = Math.round(position * (this.max - this.min) + this.min);

    return Math.min(this.max, Math.max(this.min, value));
  }

  private mapValueToPosition(value: number): number {
    if (this.max === this.min) {
      return 1;
    }

    const position = (value - this.min) / (this.max - this.min);

    return Math.min(1, Math.max(0, position));
  }
}
