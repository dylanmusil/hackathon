import {
  Component,
  Input,
  ChangeDetectionStrategy,
  EventEmitter,
  Output,
  ViewChild,
  ElementRef,
  OnChanges
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { Card } from '../../core/retro/model';
import { UserService } from 'src/app/core/user.service';

export interface ContentChangeEvent {
  oldContent: string;
  newContent: string;
}

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardComponent implements OnChanges {
  editing = false;
  fakeVoteCount = 0;

  private userId: string;

  @Input() card: Card;

  @Output() contentUpdated = new EventEmitter<ContentChangeEvent>();
  @Output() upvoted = new EventEmitter<void>();
  @Output() downvoted = new EventEmitter<void>();
  @Output() deleted = new EventEmitter<void>();

  @ViewChild('contentInput') set contentInput(el: ElementRef) {
    if (el) {
      el.nativeElement.focus();
    }
  }

  formGroup = this.fb.group({
    content: ['', Validators.required]
  });

  get content() {
    return this.formGroup.get('content');
  }

  get isMyCard() {
    return this.userId === this.card.userId;
  }

  constructor(private fb: FormBuilder, private userService: UserService) {
    this.userId = this.userService.getUserGuid();
  }

  ngOnChanges(): void {
    this.fakeVoteCount = this.card.votes;
  }

  edit() {
    this.editing = true;
    this.content.setValue(this.card.content || '');
  }

  delete() {
    this.deleted.emit();
  }

  save() {
    if (this.formGroup.valid) {
      this.contentUpdated.emit({
        oldContent: this.card.content,
        newContent: this.content.value
      });
    }

    this.editing = false;
  }

  upvote() {
    this.fakeVoteCount += 1;
    this.upvoted.emit();
  }

  downvote() {
    this.fakeVoteCount -= 1;
    this.downvoted.emit();
  }
}
