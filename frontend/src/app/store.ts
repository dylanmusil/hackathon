import { Subject, Observable } from 'rxjs';
import { startWith, scan, distinctUntilChanged, map, shareReplay } from 'rxjs/operators';

export class Store<T, R> {
  constructor(reducer: (state: T, action: R) => T, initialState: T) {
    this.actions$ = new Subject();
    this.state$ = this.actions$.pipe(
      startWith(initialState),
      scan(reducer),
      shareReplay(1)
    );
  }
  private state$: Observable<T>;
  private actions$: Subject<R>;

  dispatch(action: R): void {
    this.actions$.next(action);
  }

  select<S>(selector: (state: T) => S): Observable<S> {
    return this.state$.pipe(
      map(selector),
      distinctUntilChanged()
    );
  }
}
