import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewBoardPageComponent } from './new-board-page.component';

const routes: Routes = [
  {
    path: '',
    component: NewBoardPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewBoardPageRoutingModule { }
