import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { environment as env } from 'src/environments/environment';

export interface Board {
  id: string;
  title: string;
  created: Date;
}

@Injectable({
  providedIn: 'root'
})
export class NewBoardService {
  constructor(private http: HttpClient) {}

  getBoards(userGuid: string): Observable<Board[]> {
    let params = new HttpParams().set('ownerId', userGuid);

    return this.http.get<Board[]>(`${env.server}/retros`, { params: params });
  }

  createBoard(boardId: string, createdBy: string, title: string): Observable<void> {
    return this.http.post<void>(`${env.server}/retros/create`, {
      boardId,
      createdBy,
      title
    });
  }
}
