import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../core/user.service';
import { NewBoardService, Board } from './new-board.service';
import uuid from 'uuid/v4';

@Component({
  selector: 'app-new-board-page',
  templateUrl: './new-board-page.component.html',
  styleUrls: ['./new-board-page.component.css']
})
export class NewBoardPageComponent implements OnInit {
  userGuid: string;
  boards: Board[];
  boardTitle: string;

  constructor(private userService: UserService, private newBoardService: NewBoardService, private router: Router) {}

  ngOnInit() {
    this.userGuid = this.userService.getUserGuid();
    this.newBoardService.getBoards(this.userGuid).subscribe(boards => {
      this.boards = boards.sort((x, y) => {
        return new Date(y.created).getTime() - new Date(x.created).getTime();
      });
    });
  }

  createBoard() {
    const boardGuid = uuid();
    this.newBoardService.createBoard(boardGuid, this.userGuid, this.boardTitle).subscribe(() => {
      this.router.navigate(['', boardGuid]);
    });
  }
}
