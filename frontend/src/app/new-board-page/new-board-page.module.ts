import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NewBoardPageRoutingModule } from './new-board-page-routing.module';
import { NewBoardPageComponent } from './new-board-page.component';

@NgModule({
  declarations: [NewBoardPageComponent],
  imports: [CommonModule, NewBoardPageRoutingModule, FormsModule]
})
export class NewBoardPageModule {}
